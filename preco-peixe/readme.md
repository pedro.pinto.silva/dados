|   |   |
|---|---|
|Fonte | INE|
| Metodologia|Dados do INE sobre a captura de peixe e os valores do preço (em euros) por quilograma. Os dados não foram devidamente limpos, estando num ficheiro .xls que precisava de ser devidamente transformado para CSV|
| Artigo(s)| [A pesca da sardinha dá mais trabalho do que lucro](http://rr.sapo.pt/especial/85730/a-pesca-da-sardinha-da-mais-trabalho-do-que-lucro)|