---
title: "Exploração de dados para histórias - Ensino Secundário"
author: "Rui Barros"
date: "2019-02-18"
output:
   html_document:
     keep_md: TRUE
---

# Introdução

Neste notebook procedem-se a algumas análises para procurar histórias na base de dados do Júri Nacional de Exames.
A Base de Dados do [Ranking das Escolas](https://rr.sapo.pt/multimedia/141219/ranking-das-escolas-2018-veja-em-que-lugar-ficou-a-sua-escola), definida no documento como *df_final*, não pode ser disponibilizada.



```r
#install.packages("needs")
library(needs)
needs(readxl,tidyr,dplyr,ggplot2,Hmisc,tidyverse, rio,knitr,diptest)
```


```r
#Base de Dados dos Rankings - (não disponível)
#df_final <- import("ranking_escolas_secundário.csv")

#Todas as notas dos exames considerados válidos pela Renascença para a elaboração dos Rankings
df8 <- import("notas_exames_8_provas.csv")

#Todos os exames realizados
df <- import("todos_exames.csv")

#Chave de descodificação dos Exames
key_exames <- import("key_exames.csv")
```


# Questões de análise

[1] - Qual foi a escola fenómeno que subiu X lugares nos últimos três anos?

[2] - Qual a escola com mais 20's? Qual é a fórmula?

[3] - Relação entre habilitações literárias dos pais e notas

[4] - Ação Social Escolas Vs. Notas

[5] - Quem são os melhores alunos portugueses?


# Análise

## Qual foi a escola fenómeno que subiu X lugares nos últimos três anos?

*Df_final não disponível, pelo que o código não pode ser reproduzido*


```r
#df_fenomeno <- filter(df_final,Rank_2016 <= Rank_2015 & Rank_2017 <= Rank_2016 & rank <= Rank_2017)

#df_fenomeno$var_17_16 <- df_fenomeno$Rank_2016 - df_fenomeno$Rank_2017

#df_fenomeno$var_16_15 <- df_fenomeno$Rank_2015 - df_fenomeno$Rank_2016

#maior subida
#df_fenomeno$subidas_15.18 <- df_fenomeno$variacao_ano_ant + df_fenomeno$var_17_16 + df_fenomeno$var_16_15

#df_fenomeno <- arrange(df_fenomeno, desc(subidas_15.18))

#df_fenomeno <- df_fenomeno[c("Nome","Distrito_ok","Concelho_ok","PubPriv","nota_media","rank","Nota_Média_8_2017","Rank_2017","Media_8_2016","Rank_2016","Media_8_2015","Rank_2015")]

#kable(df_fenomeno[1:5,], caption = "As escolas que mais subiram nos rankings dos últimos anos")
```

Tanto o Instituto Inácio Loyola/Colégio da Imaculada Conceição (Privado) como a Escola Secundária de Vouzela (Pub) parecem-me bons pontos de reportagem.

As duas escolas escalaram da cauda do Ranking RR em 2015 para o top 50 das escola. Que medidas foram aplicadas? O que está a acontecer aqui? Se calhar a Escola pública neste aspecto pode ser mais interessante. Olhar para o plano educativo

No caso do Instituto Inácio Loyola, a média de exames subiu de 9.58 (2015) para 11.99(2018). Uma subida de 2.41 valores na média dos 8 exames mais comuns.

No caso da Escola Secundária de Vouzela, esta subida foi de 2.29 valores - (9.87 em 2015 e 12.16 em 2018)

**[Peça Final](https://rr.sapo.pt/noticia/141331/uma-escola-secundaria-no-meio-do-campo-aqui-e-so-paz)**



## [2] - A escola com mais 20’s. Qual a formula?


```r
notas_20 <- filter(df8, Class.Exam == 200)
notas_20_count <- aggregate(Class.Exam ~ Nome, notas_20,length)
notas_20_count <- arrange(notas_20_count, desc(Class.Exam))


kable(notas_20_count[1:7,], caption = "As escolas que mais subiram nos rankings dos últimos anos")
```



Table: As escolas que mais subiram nos rankings dos últimos anos

Nome                              Class.Exam
-------------------------------  -----------
Colégio da Rainha Santa Isabel             6
Colégio Moderno                            5
Escola Secundária Camões                   5
Colégio Casa Mãe                           4
Escola Secundária da Maia                  4
Externato Ribadouro                        4
Salesianos de Lisboa                       4

A escola com mais notas 20 num dos 8 exames foi o Colégio da Rainha Santa Isabel. Foram 6 "20's": 5 no exame de Física e Química e 1 a Matemática A. Esta é a segunda Escola no Ranking RR.

A escola pública com mais 20's foi a Escola Secundária Camões, com 4 20's a História e um a Matemática A.


### [2.1] - A escola com mais 0’s


```r
notas_0 <- filter(df8, Class.Exam <10)
notas_0_count <- aggregate(Class.Exam ~ Nome, notas_0,length)
notas_0_count <- arrange(notas_0_count, desc(Class.Exam))


kable(notas_0_count[1:7,], caption = "")
```



Table: 

Nome                                              Class.Exam
-----------------------------------------------  -----------
Escola Secundária Camões                                   6
Escola Secundária Manuel Cargaleiro                        5
Escola Secundária de José Afonso, Loures                   4
Escola Secundária Maria Amália Vaz de Carvalho             4
Escola Secundária Seomara da Costa Primo                   4
Colégio de Lamas                                           3
Escola Básica e Secundária da Sé, Lamego                   3

### [2.2] - Quantos 20 houve a história


```r
notas_hist <- filter(df8, Class.Exam ==200 & Exames_ok == "História A")
```



## [3] - Relação entre habilitações literárias dos pais e notas

*Df_final não disponível, pelo que o código não pode ser reproduzido*


```r
#df_notas <- filter(df_final,!is.na(rank) & !is.na(Habilitação.das.Mães.no.Secundário..média.do.n.º.de.anos.da.habilitação.) & !is.na(Habilitação.dos.Pais.no.3.º.Ciclo..média.do.n.º.de.anos.da.habilitação.))

#x <- df_notas$nota_media
#y <-df_notas$Habilitação.das.Mães.no.Secundário..média.do.n.º.de.anos.da.habilitação. 

#ggplot(df_notas, aes(x=nota_media, y=Habilitação.das.Mães.no.Secundário..média.do.n.º.de.anos.da.habilitação.)) + geom_point() +
#      geom_text(label=df_notas$Escola)



#y <- df_notas$Habilitação.dos.Pais.no.3.º.Ciclo..média.do.n.º.de.anos.da.habilitação.

#plot(x,y,xlab="Média de notas", ylab= "nº de anos de escolaridade (pais)",abline(lm(x~y)))

#ggplot(df_notas, aes(x=nota_media, y=Habilitação.dos.Pais.no.3.º.Ciclo..média.do.n.º.de.anos.da.habilitação.)) + geom_point() +
#      geom_text(label=df_notas$Escola)
```




```r
#df_notas$distance_pais <- sqrt((x-20)^2 + (df_notas$Habilitação.dos.Pais.no.Secundário..média.do.n.º.de.anos.da.habilitação.)^2)

#df_notas$distance_maes <- sqrt((x-20)^2 + (df_notas$Habilitação.das.Mães.no.Secundário..média.do.n.º.de.anos.da.habilitação.)^2)
```


Escolas que contrariam a tendência:

- Escola Secundária de Alpendurada - é a escola com a melhor média de notas para as habilitações literárias do pais (ie, é a escola que se aproxima mais do cenário em que alunos com pais com escolaridade 0 têm uma média de 20).

-Escola Básica e Secundária de Felgueiras, Pombeiro - é a escola com a melhor média de notas para as habilitações literárias das mães.

**[Peça Final](https://rr.sapo.pt/noticia/141452/escola-de-felgueiras-e-a-publica-que-mais-contribui-para-o-sucesso-dos-alunos-o-segredo-somos-uma-familia)**




## [4] Ação Social Escolas Vs. Notas

*Df_final não disponível, pelo que o código não pode ser reproduzido*


```r
#df_notas <- filter(df_final,!is.na(rank) & !is.na(com_ase))

#x <- df_notas$nota_media
#y <-df_notas$com_ase

#plot(x,y,xlab="Média de notas", ylab= "% de alunos com ASE", abline(lm(x~y)))
```

Provavelmente porque este dado - % de alunos com ASE - está diluído e não por prova, não há grande relação entre média de notas % de alunos com ASE (não esquecer também que não há dados de contexto para privadas, que têm as notas mais altas...)


## [5] - Retrato dos melhores e piores alunos portugueses

A nossa base de dados não identifica o mesmo aluno que faz vários exames - ou seja, não sabemos se o 15 a Matemática na escola X é do aluno que teve 19 a Português nessa escola.

No entanto os dados de contexto conseguem dizer-nos mais alguma coisa. A ideia aqui era pegar no exame de Português (exame obrigatório a todos os ramos) e filtrar pelos alunos que tiveram mais de 18. 
- Quantos são alunos das privadas e das públicas?
- Das públicas, de que concelhos são?
- Em quantas escolas estudam?


```r
#check if pt é o exame com mais provas
x <- aggregate(Class.Exam ~ Exame, df,length)

data_pt <- filter(df, Exame == 639)
nrow(data_pt)
```

```
## [1] 96042
```

```r
data_best <- filter(data_pt, Class.Exam >= 180 & Distrito_ok != "Estrangeiro")

nrow(data_best)
```

```
## [1] 533
```



```r
x <- aggregate(Exame~PubPriv,data_best,length)

y <- filter(data_best, PubPriv == "PRI")
```



```r
#mulheres vs homens
x <- aggregate(Exame~Sexo,data_best,length)
```


```r
#distrito
y <- aggregate(Exame~Distrito_ok,data_best,length)
export(y,"casos_18_distritos.csv","csv")
x <- import("casos_18_nut2.csv")
x <- aggregate(Exame~nut2,x,sum)

sum(x$Exame)
```

```
## [1] 533
```


```r
x <- import("casos_18_nut2.csv")
x <- filter(x,nut2 == "Norte")
```


```r
x <- import("casos_18_nut2.csv")
x <- aggregate(Exame~nut2+Concelho_ok,x,sum)

x <- filter(x, nut2 =="Lisboa")
sum(x$Exame)
```

```
## [1] 128
```


```r
x <- import("casos_18_nut2.csv")
```


```r
#filtro por interioridade

interior <- filter(y,Distrito_ok == "Bragança" | Distrito_ok == "Vila Real" | Distrito_ok == "Viseu"| Distrito_ok == "Guarda" | Distrito_ok == "Castelo Branco"| Distrito_ok == "Santarém"| Distrito_ok == "Portalegre"| Distrito_ok == "Évora"| Distrito_ok == "R. A. Açores"| Distrito_ok == "R. A. Madeira")

litoral <- filter(y,Distrito_ok == "Viana do Castelo" | Distrito_ok == "Braga" |Distrito_ok == "Porto" | Distrito_ok == "Aveiro" |Distrito_ok == "Coimbra" | Distrito_ok == "Leiria" |Distrito_ok == "Lisboa" |Distrito_ok == "Setúbal" | Distrito_ok == "Beja" | Distrito_ok == "Faro" )


sum(interior$Exame)
```

```
## [1] 106
```

```r
sum(litoral$Exame)
```

```
## [1] 427
```

```r
sum(interior$Exame) + sum(litoral$Exame) == 533
```

```
## [1] TRUE
```


**[Peça Final](http://rr.sapo.pt/melhores-alunos/)**











