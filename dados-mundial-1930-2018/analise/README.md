---
title: 'Dados jogos do Mundial (1930-2014)'
output:
  html_document:
    df_print: paged
---

# Dados

Dados recolhidos através de um script que recolhia todos os dados do Site da FIFA entre 1950 e 2018.

Os dados foram inicialmente recolhidos através de um script em bash e depois unificados num único ficheiro csv  que limpou campeonatos de futebol de praia, futsal, sub17, sub20, também recolhidos do site da FIFA.


```{bash}
#!/bin/bash

# based on traffic to/from : http://www.fifa.com/live-scores/international-tournaments/fixtures-results/index.html

for idx in `seq 1930 2018`;
do
	if [ ! -f "$idx.json" ]; then
		DL_LINK="http://data.fifa.com/livescores/en/internationaltournaments/matches/m/bydaterange/$idx-1-01/$((idx + 1))-1-01"
		curl -f $DL_LINK -o $idx.json
		sed -i.bak -e "s/^_matchesByDateRangeCallback(//" -e "s/)$//" $idx.json
		rm -rf *.bak
	fi
done
```


```{r}
#libraries
library(jsonlite)
# prepare the R environment
if (!require("pacman")) install.packages("pacman")
pacman::p_load(
  dplyr,            # Data munging functions
  data.table,       # Feature engineering
  ggplot2,          # Graphics
  scales,           # Time formatted axis
  readr,            # Reading input files
  stringr,          # String functions
  Amelia,           # missing data evaluation
  corrplot,         # correlation plots
  vcd               # Visualizing discrete distributions
)
    
# set options for plots
options(repr.plot.width=6, repr.plot.height=6)
```



```{r}
library(jsonlite)
df <- fromJSON("matches.json",flatten = TRUE)

#dados sobe a associação interncacional
conf <- read.csv("teams.csv")

#dados grupos
grp <- read.csv("wc2018qualified.csv")

```

Foram eliminados possíveis dados repetidos e o formato da data foi transformado:

```{r}
# eliminate any duplicates that may exist in the dataset
matches <- df %>%
  distinct(.keep_all = TRUE, date, team1, team2)

# the date field is formatted as a string (e.g. 19560930) - transform that into R date
matches$date <- as.POSIXct(strptime(matches$date, "%Y%m%d"), origin="1960-01-01", tz="UTC")

# generate an id column for future use (joins etc)
matches$match_id = seq.int(nrow(matches))
```

Como esta base de dados incluía ainda os jogos de qualificação para os mundiais, bem como todas as competições dos torneios continentais (Europeu, Copa América).

```{r}
wc_matches <- filter(matches, grepl('FIFA World Cup', CupName))
wc_matches <- filter(wc_matches, !grepl('Qualifiers', CupName))
```


```{r}
count_cups <- as.data.frame(table(wc_matches$CupName))
```


Assim, o dataframe wc_matches corresponde a todos os jogos disputados nas seguintes copas.

```{r}
write.csv(wc_matches, file = "wc_matches.csv", sep =",")
```


```{r}
print(count_cups)
```

# Questões

- Como evoluiu a média de golos por mundial?
- Qual foi o maior festival de golos? Em que mundial e entre que equipas?
- Qual foi a maior goleada?
- Quantos jogos foram vencidos nas grandes penalidades?
- Quantos jogos só foram resolvidos no tempo extra?
- Evolução do número de golos da selecção portuguesa nestes anos?


# Análise de dados

## Como evoluiu a média de golos por mundial?

```{r}
# how many goals have been scored per game over the years? 
wc_goalspgame<- wc_matches %>%
  dplyr::group_by(year = year(date)) %>%
  dplyr::summarize(
    totalgames = n(),
    totalgoals = sum(team1Score + team2Score),
    goalspergame = totalgoals / totalgames
    ) 

print(wc_goalspgame)

wc_goalspgame %>%
  ggplot(mapping = aes(x = year, y = goalspergame)) +
    geom_point() +
    geom_smooth(method = "loess") + ggtitle("Golos por jogo ao longo do tempo")
```

O campeonato do mundo de 1954  - FIFA World Cup Switzerland - foi aquele que registou o maior número de golos por jogo: foram 140 golos em apenas 26 jogos.

No entanto, se considerarmos o formato actual do Mundial (com 64 jogos), que dura desde 1998, só no último mundial (2014) é que se conseguiu igualar o recorde de mais golos por competição. Foram 171 golos marcados, e aconteceu logo no primeiro ano da competição com esse formato, em 1998.


## Qual foi o maior festival de golos? (marcados no tempo regulamentar)

```{r}
wc_matches$total_goals <- wc_matches$team1Score + wc_matches$team2Score

arrange(wc_matches,desc(total_goals))

```

Nos campeonatos analisados o Autria-Suiça de 1954 foi o maior festival de golos de sempre.

# Qual foi a maior goleada?

```{r}
wc_matches$goleada <-abs(wc_matches$team1Score - wc_matches$team2Score)

arrange(wc_matches,desc(goleada))

```

Não há uma só grande goleada, mas sim duas grandes goleadas, as duas com uma diferença de 9 golos:

- O Hungria - Coreia do Sul, no 1954 FIFA World Cup Switzerland, que terminou com a Hungria a golear 9-0.

- O Jugoslávia - Zaire, no 1974 FIFA World Cup Germany™, que também terminou 9-0

##Quantos jogos foram vencidos nas grandes penalidades?

```{r}
penalty <- filter(wc_matches, grepl('Win on penalty', statText))

nrow(wc_matches)
print(nrow(penalty))

nrow(penalty)/nrow(wc_matches)
    
```

```{r}

penalty$def_penalty <- abs(penalty$team1PenScore - penalty$team2PenScore)
arrange(penalty,desc(def_penalty))

```

Apenas 26 em 783 foram decididos nas grandes penalidades.
Destes, o prémio pior pontaria no momento importante vai para a equipa da casa no Germany FR - México, no 1986 FIFA World Cup Mexico™. Foi 4-1 nos penaltis.

## Quantos jogos só foram resolvidos no tempo extra?

```{r}
extra <- filter(wc_matches, grepl('TRUE', WinExtraTime))

nrow(wc_matches)
print(nrow(extra))

nrow(extra)/nrow(wc_matches)
    
```

Apenas 23 em 783 foram decididos no tempo extra. 


# Seleção Portuguesa

## Média de golos da seleção portuguesa

```{r}
wc_portugal <- subset(wc_matches, team1 == "POR" | team2 == "POR")
wc_portugal1 <- subset(wc_matches, team1 == "POR")
wc_portugal2 <- subset(wc_matches, team2 == "POR")

wc_portugal1$gols_pt <- wc_portugal1$team1Score
wc_portugal2$gols_pt <- wc_portugal2$team2Score

wc_portugal_f <- rbind(wc_portugal1,wc_portugal2)

if (nrow(wc_portugal) == nrow(wc_portugal_f)){
    wc_portugal <- wc_portugal_f
}

```


```{r}
sum(wc_portugal$gols_pt)
```
Portugal já marcou 43 golos no Mundial.



```{r}
# how many goals have been scored per game over the years? 
wc_goalspgame_pt<- wc_portugal %>%
  dplyr::group_by(year = year(date)) %>%
  dplyr::summarize(
    totalgames = n(),
    totalgoals = sum(team1Score + team2Score),
    goalspergame = totalgoals / totalgames
    ) 

print(wc_goalspgame_pt)

wc_goalspgame_pt %>%
  ggplot(mapping = aes(x = year, y = goalspergame)) +
    geom_point() +
    geom_smooth(method = "loess") + ggtitle("Golos por jogo ao longo do tempo")
```

Foi no ano de estreia de Portugal no Mundial que marcamos mais golos - 25, em 6 jogos. Foi nesse ano que registamos também a melhor média de golos por jogo - 4.1.


```{r}
campeonatos_pt <- as.data.frame(table(wc_portugal$CupName))
print(campeonatos_pt)

```
Em 26 jogos distribuídos por 6 taças do Mundo em que Portugal participou, a selecção só precisou de ir uma vez a penaltis. Foi no Inglaterra-Portugal, no 2006 FIFA World Cup Germany™.

Apesar do nosso europeu, nunca fomos a tempo suplementar (???).

## maiores goleadas da seleção:

```{r}
arrange(wc_portugal,desc(gols_pt))

```

Os 7-0 de Portugal contra a Coreia do Norte, no 2010 FIFA World Cup South Africa™, continuam a ser o melhore registo da selecção no Mundial.




